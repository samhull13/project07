package edu.mcckc.driver;

import edu.mcckc.gui.TextFrame;

import javax.swing.*;
/**
 Sandy Hull
 project07
 May 3, 2017
 */

public class Main
{

    public static void main(String[] args)
    {
        //create frame and set visible
        TextFrame frmApp = new TextFrame();
        frmApp.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frmApp.setSize(900, 400);
        frmApp.setVisible(true);
    }
}
