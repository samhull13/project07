package edu.mcckc.gui;

import edu.mcckc.domain.TextManager;
import org.apache.log4j.Logger;

import javax.swing.*;
import java.awt.*;

/**
 Sandy Hull
 project07
 May 3, 2017
 */

public class TextViewPanel extends JPanel
{
    private JLabel lblCaptionVowels;
    private JLabel lblOutputVowels;
    private JLabel lblCaptionConsonants;
    private JLabel lblOutputConsonants;
    private JLabel lblCaptionNumbers;
    private JLabel lblOutputNumbers;
    private JLabel lblCaptionSpaces;
    private JLabel lblOutputSpaces;
    private JLabel lblCaptionPunctuation;
    private JLabel lblOutputPunctuation;

    private TextManager manager;

    public void setManagerReference(TextManager manager)
    {
        this.manager = manager;
    }


    public TextViewPanel()
    {
        //create labels for view panel
        lblCaptionVowels = new JLabel("Vowel Count: ");
        lblOutputVowels = new JLabel("");
        lblCaptionConsonants = new JLabel("Consonant Count: ");
        lblOutputConsonants = new JLabel("");
        lblCaptionNumbers = new JLabel("Digit Count: ");
        lblOutputNumbers = new JLabel("");
        lblCaptionSpaces = new JLabel("Space Count: ");
        lblOutputSpaces = new JLabel("");
        lblCaptionPunctuation = new JLabel("Punctuation Count: ");
        lblOutputPunctuation = new JLabel("");

        //layout and place on panel
        setLayout(new GridLayout(5, 2));
        add(lblCaptionVowels);
        add(lblOutputVowels);

        add(lblCaptionConsonants);
        add(lblOutputConsonants);

        add(lblCaptionNumbers);
        add(lblOutputNumbers);

        add(lblCaptionSpaces);
        add(lblOutputSpaces);

        add(lblCaptionPunctuation);
        add(lblOutputPunctuation);

        Logger.getLogger(this.getClass()).debug("EAST FRAME CREATED");
    }

public void processInputString(int vowelCt, int consonantCt, int numberCt, int spaceCt, int punctuationCt)
    {
        //place calculated values on the panel when button is pushed
        lblOutputVowels.setText(String.format("%d", vowelCt));
        lblOutputConsonants.setText(String.format("%d", consonantCt));
        lblOutputNumbers.setText(String.format("%d", numberCt));
        lblOutputSpaces.setText(String.format("%d", spaceCt));
        lblOutputPunctuation.setText(String.format("%d", punctuationCt));
    }
}
