package edu.mcckc.gui;

import edu.mcckc.domain.TextManager;
import org.apache.log4j.Logger;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
/**
 Sandy Hull
 project07
 May 3, 2017
 */
public class TextEntryPanel extends JPanel implements ActionListener
{
    private JDataEntry deInput;
    private JButton btnSubmit;
    private  JPanel row1;
    private  JPanel row2;
    private TextViewPanel pnlView;
    private TextManager manager;

    public void setManagerReference(TextManager manager)
    {
        this.manager = manager;
    }

    public void setPanelDisplayReference(TextViewPanel pnlView)
    {
        this.pnlView = pnlView;
    }


    public TextEntryPanel()
    {
        //create label and field for string entry
        deInput = new JDataEntry("Enter Text: ", 42, "");

        //create submit button
        btnSubmit = new JButton("Submit");
        btnSubmit.setActionCommand("sub");
        btnSubmit.addActionListener(this);

        //create first row with string input
        row1 = new JPanel();
        row1.setLayout(new GridLayout(1, 1));
        row1.add(deInput);

        //create second row with submit button
        row2 = new JPanel();
        row2.setLayout(new FlowLayout());
        row2.add(btnSubmit);

        //layout panel and add fields and button
        setLayout(new GridLayout(2, 1));
        add(row1);
        add(row2);

    }

    //button action
    @Override
    public void actionPerformed(ActionEvent e)
    {
        if (e.getActionCommand().toLowerCase().equals("sub"))
            Logger.getLogger(this.getClass()).debug("SUBMIT BUTTON HAS BEEN PUSHED");
        {
            try
            {
                //call method from manager to parse string and set counts
                manager.processInputString(deInput.getText());

                //call method from the view panel to display the counts
                pnlView.processInputString(manager.getVowelCount(), manager.getConsonantCount(),
                        manager.getNumberCount(), manager.getSpaceCount(), manager.getPunctuationCount()
                );
            }

            catch (Exception ex)
            {
                deInput.setText("");
                Logger.getLogger(this.getClass()).debug("ERROR WITH BUTTON ACTION");
            }

        }
    }




}
