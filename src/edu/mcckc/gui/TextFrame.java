package edu.mcckc.gui;

import edu.mcckc.domain.TextManager;
import javax.swing.*;
import java.awt.*;

/**
 Sandy Hull
 project07
 May 3, 2017
 */

public class TextFrame extends JFrame
{
    private TextEntryPanel pnlEntry;
    private TextViewPanel pnlView;
    private TextManager manager;

    public TextFrame()
    {
        //create panels and manager
        pnlEntry = new TextEntryPanel();
        pnlView = new TextViewPanel();
        manager = new TextManager();

        //send the view panel to the entry panel as a reference
        pnlEntry.setPanelDisplayReference(pnlView);
        pnlEntry.setManagerReference(manager);
        pnlView.setManagerReference(manager);

        //layout the side panels
        add(pnlEntry, BorderLayout.WEST);
        add(pnlView, BorderLayout.EAST);
    }
}
