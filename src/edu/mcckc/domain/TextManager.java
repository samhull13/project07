package edu.mcckc.domain;

import org.apache.log4j.Logger;

import java.util.ArrayList;

/**
 Sandy Hull
 project07
 May 3, 2017
 */
public class TextManager
{
    private String inputString;
    private String upperInputString;
    private ArrayList<String> lstVowels;
    private ArrayList<String> lstPunctuation;
    private int vowelCount;
    private int punctuationCount;
    private int consonantCount;
    private int numberCount;
    private int spaceCount;

    private boolean isVowel;
    private boolean isPunctuation;
    private boolean isNumber;
    private boolean isSpace;

    public TextManager()
    {
        lstVowels = new ArrayList<String>();
        lstPunctuation = new ArrayList<String>();
        initEverything();
    }

    private void initEverything()
    {
        initCounts();
        initBooleanFlags();
        initVowels();
        initPunctuation();
    }

    private void initCounts()
    {
        vowelCount = 0;
        punctuationCount = 0;
        consonantCount = 0;
        numberCount = 0;
        spaceCount = 0;
    }

    private void initBooleanFlags()
    {
        isPunctuation = false;
        isSpace = false;
        isVowel = false;
        isNumber = false;
    }

    private void initVowels()
    {
        lstVowels.add("A");
        lstVowels.add("E");
        lstVowels.add("I");
        lstVowels.add("O");
        lstVowels.add("U");
        lstVowels.add("Y");
    }

    private void initPunctuation()
    {
        lstPunctuation.add(",");
        lstPunctuation.add(".");
        lstPunctuation.add("?");
        lstPunctuation.add("!");
        lstPunctuation.add(";");
        lstPunctuation.add(":");
    }

    public void processInputString(String inputString)
    {
        Logger.getLogger(this.getClass()).debug(String.format("STRING TO PARSE IS: %s", inputString));
        Logger.getLogger(this.getClass()).debug(String.format("STRING LENGTH IS: %s", inputString.length()));

        initCounts();

        for (int i = 0; i < inputString.length(); i++)
        {
            //set string  to upper case for vowel check
            upperInputString = inputString.toUpperCase();
            Logger.getLogger(this.getClass()).debug(String.format("CHECKING LETTER %s ", upperInputString.substring(i, i+1)) );

            initBooleanFlags();

            //check for vowels
            processVowel(upperInputString.substring(i, i+1));

            //check for punctuation
            processPunctuation(inputString.substring(i, i+1));

            //check for spaces
            processSpace(inputString.substring(i, i+1));

            //check for numbers
            processNumeric(inputString.substring(i, i + 1));

            if( !isVowel   &&  !isPunctuation  && !isSpace && !isNumber      )
            {
                consonantCount++;
                Logger.getLogger(this.getClass()).debug(String.format("CONSONANT COUNT IS: %d", consonantCount) );
            }
        }

        //set variables to call later for display
        setVowelCount(vowelCount);
        setPunctuationCount(punctuationCount);
        setSpaceCount(spaceCount);
        setNumberCount(numberCount);
        setConsonantCount(consonantCount);
    }


    private void processVowel(String stringToken)
    {

        for (String temp : lstVowels) {

            if(temp.equals(stringToken))
            {
                isVowel = true;
                vowelCount++;
                Logger.getLogger(this.getClass()).debug(String.format("VOWEL COUNT IS: %d", vowelCount) );
            }
        }
    }

    private void processPunctuation(String stringToken)
    {
        for (String temp : lstPunctuation) {

            if(temp.equals(stringToken))
            {
                isPunctuation = true;
                punctuationCount++;
                Logger.getLogger(this.getClass()).debug(String.format("PUNCTUATION COUNT IS: %d", punctuationCount) );
            }
        }
    }

    private void processSpace(String stringToken)
    {

        if (stringToken.equals(" ")) {
            isSpace = true;
            spaceCount++;

            Logger.getLogger(this.getClass()).debug(String.format("SPACE COUNT IS: %d", spaceCount) );
        }

    }

    private void processNumeric(String stringToken)
    {
        if (Character.isDigit(stringToken.charAt(0)))
        {
            isNumber = true;
            numberCount++;
            Logger.getLogger(this.getClass()).debug(String.format("NUMBER COUNT IS: %d", numberCount) );
        }
    }

    public int getConsonantCount()
    {
        return consonantCount;
    }

    public int getVowelCount()

    {
        return vowelCount;
    }

    public int getNumberCount()

    {
        return numberCount;
    }

    public int getPunctuationCount()

    {
        return punctuationCount;
    }

    public int getSpaceCount()

    {
        return spaceCount;
    }

    public void setVowelCount(int vowelCount)
    {
        this.vowelCount = vowelCount;
    }

    public void setPunctuationCount(int punctuationCount)
    {
        this.punctuationCount = punctuationCount;
    }

    public void setConsonantCount(int consonantCount)
    {
        this.consonantCount = consonantCount;
    }

    public void setNumberCount(int numberCount)
    {
        this.numberCount = numberCount;
    }

    public void setSpaceCount(int spaceCount)
    {
        this.spaceCount = spaceCount;
    }
}
